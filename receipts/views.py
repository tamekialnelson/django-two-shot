from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm

# view that will get all of the instances of the Receipt model


@login_required
def receipt_list(request):
    list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": list,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == 'GET':
        form = ReceiptForm()
    else:
        form = ReceiptForm(request.POST)
        if form.is_valid():
            Receipt = form.save(False)
            Receipt.purchaser = request.user
            Receipt.save()
            return redirect('home')
    context = {
        'form': form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    expense_category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories_list": expense_category,
    }
    return render(request, "categories/list.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "account_list": accounts,
    }
    return render(request, "accounts/list.html", context)


@login_required
def create_category(request):
    if request.method == 'GET':
        form = CategoryForm()
    else:
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect('category_list')
    context = {
        'form': form,
    }
    return render(request, "categories/create.html", context)


@login_required
def create_account(request):
    if request.method == 'GET':
        form = AccountForm()
    else:
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    context = {
        'form': form,
    }
    return render(request, "accounts/create.html", context)
